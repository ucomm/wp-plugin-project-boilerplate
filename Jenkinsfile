pipeline {
  agent any
  environment {
    THEME_OR_PLUGIN_SLUG = "my-project"
    // sometimes the project slug and repo slug are different
    BITBUCKET_SLUG = "my-project"
    DEV_BRANCH = "develop"
    FEATURE_BRANCH = "feature/*"
    PROD_BRANCH = "main"
  }
  // handle rsync dry runs
  parameters {
    booleanParam(
      name: 'RSYNC_DRY_RUN',
      defaultValue: true,
      description: 'Toggles the rsync --dry-run flag'
    )
  }
  stages {
    // checkout from git and skip if the "ci skip" message is present in the commit
    stage('Checkout') {
      steps {
        // do not delete builds. there's a breaking issue there
        // https://issues.jenkins.io/browse/JENKINS-66843
        scmSkip(deleteBuild: false)
        // relies on the global notification library
        // https://bitbucket.org/ucomm/jenkins-send-notifications/src/main/
        sendNotifications 'STARTED'
      }
    }
    stage("Prepare Build Assets") {
      // run these tasks at the same time
      parallel {
        stage('NPM') {
          steps {
            catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
              sh "${WORKSPACE}/ci-scripts/npm.sh"
            }
          }
        }
        stage('Composer') {
          steps {
            catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
              sh "${WORKSPACE}/ci-scripts/composer.sh"
            }
          }
        }
      }
      post {
        failure {
          sendNotifications("FAILURE - Prepare Build Assets")
        }
      }
    }
    stage('Dev Pushes') {
      // set environment variables 
      environment {
        FILENAME = "$GIT_BRANCH"
        SITE_DIRECTORY = "edu.uconn.somesite.dev"
      }
      when {
        anyOf {
          branch "${DEV_BRANCH}";
          branch "${FEATURE_BRANCH}"
        }
      }
      parallel {
        stage('Push to staging0') {
          steps {
            catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
              sh "${WORKSPACE}/ci-scripts/dev-push.sh"
            }
          }
        }
        // run this if needed in parallel
        // stage('Archive dev to bitbucket') {
        //   steps {
        //     catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
        //       sh "${WORKSPACE}/ci-scripts/bitbucket-archive.sh"
        //     }
        //   }
        // }
      }
      post {
        failure {
          sendNotifications("FAILURE - Dev Pushes")
        }
      }
    }
    stage('Staging Push') {
      environment {
        SITE_DIRECTORY = "edu.uconn.somesite.staging"
      }
      when {
        branch "${PROD_BRANCH}"
      }
      steps {
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          sh "${WORKSPACE}/ci-scripts/dev-push.sh"
        }
      }
      post {
        failure {
          sendNotifications("FAILURE - Staging Push")
        }
      }
    }
    stage('Prod Push') {
      environment {
        SITE_DIRECTORY = "edu.uconn.somesite"
      }
      when {
        buildingTag()
      }
      steps {
        sh "${WORKSPACE}/ci-scripts/prod-push.sh"
      }
      post {
        failure {
          sendNotifications("FAILURE - Push to prod")
        }
        success {
          // NB - don't use script blocks very much.
          // I'm just adding this so th comm0-updates channel doesn't get flooded on dry runs
          script {
            if (env.RSYNC_DRY_RUN == "false") {
              sendNotifications("SUCCESS", "#comm0-updates", [
                [
                  type: "section",
                  text: [
                    type: "mrkdwn",
                    text: ":tada: *$THEME_OR_PLUGIN_SLUG* updated"
                  ]              
                ]
              ])
              slackUploadFile(channel: "#comm0-updates", filePath: "changelog.md", initialComment:  "${THEME_OR_PLUGIN_SLUG} Changelog")
            }
          }
        }
      }
    }
    // only push tagged releases to bitbucket
    stage('Archive tag to bitbucket') {
      environment {
        FILENAME = "$TAG_NAME"
      }
      when {
        buildingTag()
      }
      steps {
        sh "${WORKSPACE}/ci-scripts/bitbucket-archive.sh"
      }
    }
  }
  post {
    // send slack notifications when the project finishes
    success {
      sendNotifications 'SUCCESSFUL'
    }
    failure {
      sendNotifications 'FAILED'
    }
    aborted {
      sendNotifications 'ABORTED'
    }
    always {
      echo "======== Cleanup ========"
      sh "rm -rf node_modules"
    }
  }
}