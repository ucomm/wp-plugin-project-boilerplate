const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')
const DependencyExtractionPlugin = require('@wordpress/dependency-extraction-webpack-plugin')

const env = process.env.NODE_ENV
const prodEnv = env === 'production' ? true : false
const buildDir = prodEnv ? 'build' : 'dev-build'
const mode = prodEnv ? 'production' : 'development'
const optimization = prodEnv ? {
  minimize: true,
  minimizer: [
    new CssMinimizerPlugin(),
    '...',
  ],
} : {}

module.exports = {
  entry: {
    index: path.resolve(__dirname, 'src', 'js', 'index.ts'),
    admin: path.resolve(__dirname, 'src', 'js', 'admin.ts'),
  },
  output: {
    path: path.resolve(__dirname, buildDir),
    filename: '[name].js',
    chunkFilename: '[name].js'
  },
  mode,
  optimization,
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: 'ts-loader',
      },
      /**
       * 
       * Use this if you intend to bundle style assets as well.
       * Make sure to install the appropriate loaders etc...
       * 
       */
      {
        test: /\.s?css$/i,
        exclude: /node_modules/,
        use: [
          MiniCssExtractPlugin.loader,
          { loader: 'css-loader', options: { sourceMap: true } },
          { loader: 'sass-loader', options: { sourceMap: true } }
        ]
      },
      {
        test: /\.(jpg|jpeg|png|svg)$/i,
        type: 'asset/resource'
      },
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.ts', '.tsx', '.css', '.scss']
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css'
    }),
    new ForkTsCheckerWebpackPlugin(),
    new DependencyExtractionPlugin({
      useDefaults: true
    })
  ],
  watchOptions: {
    ignored: /node_modules/
  }
}