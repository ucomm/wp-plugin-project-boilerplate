<?php

namespace Boilerplate\Database;

/**
 * Prepare the database
 * - Keep track of the database version
 * - Allow access to the WP db functions without exposing them to the global scope
 * 
 * This class allows encapsulates the global WP database. That way it's not completely exposed at the global scope. It can also be extended so that you can use different classes to do things like:
 * - create and manage tables
 * - perform queries and inserts
 * - any other db operation... without cluttering this class too much.
 * 
 */
class Database
{
  protected $db;
  protected $dbVersion;

  public function __construct()
  {
    global $wpdb;
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    $this->db = $wpdb;
    $this->dbVersion = '1.0.0';
  }

  public function getDB()
  {
    return $this->db;
  }

  public function getDBVersion()
  {
    return $this->dbVersion;
  }

  /**
   * An example of how to use this class to insert a row to the database.
   * This way we can call this method from a different class method
   * 
   * Insert a single row into a table
   *
   * @param string $tableName
   * @param array $data
   * @return int|false — The number of rows inserted, or false on error.
   */
  public function insertRow(string $tableName, array $data) {
    return $this->db->insert($tableName, $data);
  }

  /**
   * Store the database version as an option in wp_options.
   * This way it can be easily checked and compared.
   *
   * @return bool
   */
  protected function setDBVersion(): bool
  {
    $optionAdded = false;
    // if (!get_option('boilderplate_db_version')) {
      // $optionAdded = add_option('boilderplate_db_version', $this->dbVersion);
    // }
    return $optionAdded;
  }
}