<?php

namespace Boilerplate\Admin\Settings;

class SettingsManager {
  protected $exampleOption;
  protected $menuSlug;

  public function __construct() {
    $this->menuSlug = 'boilerplate-settings-menu';
    $this->exampleOption = $this->setOption(BOILERPLATE_SETTINGS_EXAMPLE, '');
  }

  public function getMenuSlug(): string {
    return $this->menuSlug;
  }

  public function setOption(string $optionName, $optionValue = null) {
    if (!get_option($optionName)) {
      update_option($optionName, $optionValue);
    }
    return get_option($optionName);
  }
}