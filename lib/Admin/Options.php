<?php

namespace Boilerplate\Admin\Settings;

/**
 * This class allows for 
 * - the creation of settings in the wp_options table
 * - getting and displaying the form to modify settings.
 */
class Options extends SettingsManager {
  public function __construct() {
    parent::__construct();
  }

  public function init() {
    $this->registerSettings();
    $this->addSettingsSections();
    $this->addSettingsFields();
  }

  public function registerSettings() {
    register_setting(
      BOILERPLATE_SETTINGS,
      BOILERPLATE_SETTINGS_EXAMPLE,
    );
  }

  public function addSettingsSections() {
    add_settings_section(
      'boilerplate-settings-section',
      'Boilerplate Settings',
      [ $this, 'settingsSectionCallback' ],
      $this->getMenuSlug()
    );
  }

  public function addSettingsFields() {
    add_settings_field(
      BOILERPLATE_SETTINGS_EXAMPLE,
      'Example Setting',
      [ $this, 'settingsFieldCallback' ],
      $this->getMenuSlug(),
      'boilerplate-settings-section',
      [
        'value' => get_option(BOILERPLATE_SETTINGS_EXAMPLE, ''),
        'label_for' => BOILERPLATE_SETTINGS_EXAMPLE,
        'class' => BOILERPLATE_SETTINGS_EXAMPLE,
      ]
    );
  }

  public function settingsSectionCallback($args) {
    include PLUGIN_DIR . 'partials/admin/settings-section.php';
  }

  public function settingsFieldCallback($args) {
    $value = $args['value'];
    $labelFor = $args['label_for'];
    $class = $args['class'];
    echo "<input type='text' id='$labelFor' name='$labelFor' value='$value' class='$class' />";
    include PLUGIN_DIR . 'partials/admin/settings-field.php';
  }
}