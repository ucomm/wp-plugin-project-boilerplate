<?php

namespace Boilerplate\Admin\Settings;

class SettingsPage extends SettingsManager {

  public function __construct() {
    parent::__construct();
  }

  public function addMenuPage() {
    add_menu_page(
      __('Boilerplate Settings', 'boilerplate'),
      __('Boilerplate Settings', 'boilerplate'),
      'manage_options',
      $this->menuSlug,
      [$this, 'getParentSettingsPage'],
      'dashicons-groups'
    );
  }

  public function getParentSettingsPage() {
    if (!current_user_can('manage_options')) {
      wp_die(__('You do not have sufficient permissions to access this page.'));
    }

    if (isset($_GET['settings-updated'])) {
      add_settings_error('boilerplate_messages', 'boilerplate_message', __('Settings Saved', 'boilerplate'), 'updated');
    }

    settings_errors('boilerplate_messages');
    
    include PLUGIN_DIR . 'partials/admin/parent-settings-page.php';
  }
}