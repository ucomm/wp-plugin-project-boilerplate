<?php

namespace Boilerplate\Endpoints;

use WP_REST_Controller;

/**
 * This class helps manage REST API endpoints. The endpoint namespace and resource are centrally managed from here which makes updates easier. All classes that inherit from here will need to implement the registerRoutes method.
 * 
 * If you need it, you can easily access the database using the Database class. 
 * 
 * You can also use this as a model for extending WP_REST_Posts_Controller
 */
abstract class Controller extends WP_REST_Controller {
  protected $namespace;
  protected $resource;

  public function __construct(string $namespace = '/uconn/v1', string $resource = 'resources') {
    $this->namespace = $namespace;
    $this->resource = $resource;
  }

  /**
   * Register the routes for the API endpoints
   *
   * @return void
   */
  public function apiInit() {
    add_action('rest_api_init', [$this, 'registerRoutes']);
  }

  public function permissionCallback(): bool {
    return WP_ENVIRONMENT_TYPE === 'local' || current_user_can('delete_pages');
  }

  /**
   * Enumerate the routes to be registered in the class
   *
   * @return void
   */
  abstract function registerRoutes();
}