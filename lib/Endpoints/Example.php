<?php

namespace Boilerplate\Endpoints;

use WP_REST_Request;
use WP_REST_Server;

class Example extends Controller
{
  public function registerRoutes() {
    register_rest_route($this->namespace, '/' . $this->resource, [
      [
        'methods' => WP_REST_Server::READABLE,
        'callback' => [ $this, 'getResponse' ],
        'permission_callback' => [ $this, 'permissionCallback' ],
      ],
      [
        'methods' => WP_REST_Server::CREATABLE,
        'callback' => [ $this, 'createResponse' ],
        'permission_callback' => [ $this, 'permissionCallback' ],
      ],
      [
        'methods' => WP_REST_Server::EDITABLE,
        'callback' => [ $this, 'updateResponse' ],
        'permission_callback' => [ $this, 'permissionCallback' ],
      ],
      [
        'methods' => WP_REST_Server::DELETABLE,
        'callback' => [ $this, 'deleteResponse' ],
        'permission_callback' => [ $this, 'permissionCallback' ],
      ]
    ]);
  }

  public function getResponse() {
    $data = [
      'message' => 'hello world',
    ];

    return rest_ensure_response($data);
  }

  public function createResponse(WP_REST_Request $request) {
    $params = $request->get_params();
    $data = [
      'message' => 'POST to this endpoint',
    ];

    return rest_ensure_response($data);
  }

  public function updateResponse(WP_REST_Request $request) {
    $params = $request->get_params();
    $data = [
      'message' => 'PUT to this endpoint',
    ];

    return rest_ensure_response($data);
  }

  public function deleteResponse(WP_REST_Request $request) {
    $params = $request->get_params();
    $data = [
      'message' => 'DELETE to this endpoint',
    ];

    return rest_ensure_response($data);
  }
}

$exampleEndpoint = new Example();
$exampleEndpoint->apiInit();