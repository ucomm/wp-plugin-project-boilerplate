<?php

namespace Boilerplate\Assets;

use Boilerplate\Frontend\Shortcode;

abstract class Loader {
  protected $handle;
  protected $adminHandle;
  protected $buildDir;
  protected $shortcodeSlug;

  public function __construct() {
    $this->handle = $this->setHandle();
    $this->adminHandle = $this->setAdminHandle();
    // set the build directory based on environment
    $this->buildDir = wp_get_environment_type() === 'local' ?
      'dev-build' :
      'build';
    $this->shortcodeSlug = Shortcode::getShortcodeSlug();
  }

  /**
   * Enqueue assets when the wp_enqueue_scripts action is called
   *
   * @return void
   */
  abstract function enqueue(): void;

  /**
   * Enqueue assets when the admin_enqueue_scripts action is called
   *
   * @return void
   */
  abstract function adminEnqueue(string $hook): void;

  /**
   * Set the base handle for scripts and styles
   *
   * @return string
   */
  protected function setHandle(): string {
    return 'boilerplate';
  }

  /**
   * Set the base handle for admin scripts and styles
   *
   * @return string
   */
  protected function setAdminHandle(): string {
    return 'boilerplate-admin';
  }
}