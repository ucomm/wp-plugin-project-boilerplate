<?php

namespace Boilerplate\Assets;

use Boilerplate\Assets\Loader;

/**
 * A class to handle loading CSS assets
 */
class StyleLoader extends Loader {
  /**
   * Enqueue the prepared scripts.
   * In this example, the script will only be enqueued if the shortcode is present.
   *
   * @return void
   */
  public function enqueue(): void {
    global $post;

    if (has_shortcode($post->post_content, $this->shortcodeSlug)) {
      $this->prepareAppStyles();

      wp_enqueue_script($this->handle);
    }
  }

  /**
   * Prepare the styles by registering them. The directory for the script is determined by the environment.
   *
   * @return void
   */
  private function prepareAppStyles() {
    $assetFile = include PLUGIN_DIR . $this->buildDir . '/index.asset.php';
    $styleDeps = [];
    wp_register_style(
      $this->handle,
      PLUGIN_URL . $this->buildDir . '/index.css',
      $styleDeps,
      $assetFile['version']
    );
  }



  /**
   * This method can be used to enqueue an asset on an admin page.
   * Use the slug to filter which pages it should be used on.
   *
   * @param string $hook - the admin page's slug to enqueue on
   * @return void
   */
  public function adminEnqueue(string $hook): void {
    $settingsPage = stripos($hook, BOILERPLATE_SETTINGS);
    if ($settingsPage !== false) {
      $this->prepareAdminStyles();
      wp_enqueue_style($this->adminHandle);
    }
  }

  private function prepareAdminStyles() {
    $assetFile = include PLUGIN_DIR . '/admin.asset.php';
    $styleDeps = [];
    return wp_register_style(
      $this->adminHandle,
      PLUGIN_URL . '/admin.css',
      $styleDeps,
      $assetFile['version']
    );
  }
}