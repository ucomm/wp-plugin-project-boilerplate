<?php

namespace Boilerplate\Assets;

use Boilerplate\Assets\Loader;

/**
 * A class to handle loading JS assets
 */
class ScriptLoader extends Loader {
  /**
   * Enqueue the prepared scripts.
   * In this example, the script will only be enqueued if the shortcode is present.
   *
   * @return void
   */
  public function enqueue(): void {
    global $post;

    if (has_shortcode($post->post_content, $this->shortcodeSlug)) {
      $this->prepareAppScript();

      wp_enqueue_script($this->handle);
      // if you need to pass data to the script, use wp_add_inline_script with the 'before' position
      // wp_add_inline_script($this->handle, 'var myData = ' . json_encode($data), 'before');
    }
  }

  /**
   * This method can be used to enqueue an asset on an admin page.
   * Use the slug to filter which pages it should be used on.
   *
   * @param string $hook - the admin page's slug to enqueue on
   * @return void
   */
  public function adminEnqueue(string $hook): void {
    if (false !== stripos($hook, BOILERPLATE_SETTINGS)) {
      $this->prepareAdminScript();

      wp_enqueue_script($this->adminHandle);
      // if you need to pass data to the script, use wp_add_inline_script with the 'before' position
      // wp_add_inline_script($this->adminHandle, 'var myData = ' . json_encode($data), 'before');
    }
  }

  /**
   * Prepare the script by registering it.
   *
   * @return void
   */
  private function prepareAppScript() {
    $assetFile = include PLUGIN_DIR . $this->buildDir . '/index.asset.php';
    $scriptDeps = $assetFile['dependencies'];
    wp_register_script(
      $this->handle,
      PLUGIN_URL . $this->buildDir . '/index.js',
      $scriptDeps,
      $assetFile['version'],
      true
    );
  }

  private function prepareAdminScript() {
    $assetFile = include PLUGIN_DIR . $this->buildDir . '/admin.asset.php';
    $scriptDeps = $assetFile['dependencies'];
    wp_register_script(
      $this->adminHandle,
      PLUGIN_URL . $this->buildDir . '/admin.js',
      $scriptDeps,
      $assetFile['version'],
      true
    );
  }
}