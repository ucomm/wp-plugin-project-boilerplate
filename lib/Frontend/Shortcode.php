<?php

namespace Boilerplate\Frontend;

class Shortcode {
  public static $shortcodeSlug = 'boilerplate-shortcode';
  public $atts;

  public function __construct() {
    $this->atts = [];
  }

  public function addShortcode() {
    add_shortcode(self::$shortcodeSlug, [ $this, 'display' ]);
  }

  public function display(array $atts = []): string {
    ob_start();
    $this->setAtts($atts);
    include(PLUGIN_DIR . '/partials/public/public-display.php');
    return ob_get_clean();
  }

  public static function getShortcodeSlug(): string {
    return self::$shortcodeSlug;
  }

  private function setAtts(array $atts): array {
    $this->atts = shortcode_atts([
        'example' => 'default'
      ],
      $atts,
      self::$shortcodeSlug
    );
    return $this->atts;
  }
}