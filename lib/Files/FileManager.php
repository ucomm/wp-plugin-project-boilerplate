<?php

namespace Boilerplate\Files;

use Generator;
// use Boilerplate\Database\Database;

/**
 * 
 * This class will help manage files and file uploads. You can use it to create a custom directory in the wordpress upload directory. From there, you can do all kinds of things. If you need to write data to the database, it's recommended to use a Generator function as below. For a working example, see - https://bitbucket.org/ucomm/uconn-transfer-credit/src/main/lib/Admin/Files/FileManager.php
 * 
 */
class FileManager {
  protected $fileName;
  protected $filePath;
  protected $uploadFileName;
  protected $uploadDir;
  protected $fileUploadDir;
  // private $db;

  public function __construct() {
    $this->fileName = 'example.txt';
    $this->uploadFileName = 'upload_' . $this->fileName;
    $this->uploadDir = wp_upload_dir();
    $this->fileUploadDir = $this->uploadDir['basedir'] . '/boilerplate-uploads';
    $this->filePath = trailingslashit($this->fileUploadDir) . $this->uploadFileName;
    // $this->db = new Database();
  }

  /**
   * Get the server path to the file upload directory.
   *
   * @return string - file upload path
   */
  public function getFileUploadDir(): string {
    return $this->fileUploadDir;
  }

  /**
   * Get the server path to the file
   *
   * @return string
   */
  public function getFilePath(): string {
    return $this->filePath;
  }

  /**
   * Writes rows from the upload file into the database with the wordpress insert method.
   *
   * @return integer - A count of all the rows inserted
   */
  // public function writeFileToDatabase(): int {
  //   $count = 0;
  //   $rows = $this->yieldData();
  //   foreach ($rows as $row) {
  //     $count += $this->db->insertRow($this->tableName, [
  //       // set the rows of the table
  //     ]);
  //   }
  //   return $count;
  // }


  /**
   * In order to avoid running out of memory on the server, we can use a generator.
   * The generator yields instead of returns the value.
   * This generator creates an iterable.
   * When the result of the method is looped over "PHP will call the object's iteration methods each time it needs a value, then saves the state of the generator when the generator yields a value so that it can be resumed when the next value is required."
   *
   * reference - https://www.php.net/manual/en/language.generators.syntax.php
   * 
   * @return Generator
   */
  public function yieldData(): Generator {
    $stream = fopen($this->filePath, 'r');
    try {
      if ($stream !== false) {
        // set a falsifiable condition and then yield data that comes from the file
        while (false) {
          yield false;
        }
      }
    } finally {
      fclose($stream);
    }
  }
}