<?php

use PHPUnit\Framework\TestCase;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Brain\Monkey;

/**
 *
 *This is the base test case for all tests. It sets up the Brain Monkey framework and adds Mockery expectations to the PHPUnit assertions count. Brainmonkey will then allow you to mock WordPress functions and classes.
 * https://giuseppe-mazzapica.gitbook.io/brain-monkey/
 * 
 */
class BrainMonkeyTestCase extends TestCase
{

    // Adds Mockery expectations to the PHPUnit assertions count.
    use MockeryPHPUnitIntegration;

    protected function setUp(): void
    {
        parent::setUp();
        Monkey\setUp();
    }

    protected function tearDown(): void
    {
        Monkey\tearDown();
        parent::tearDown();
    }
}