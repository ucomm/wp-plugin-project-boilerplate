import { test, expect } from '@playwright/experimental-ct-react'
import React from 'react'
import App from '../../../../src/js/Admin/App'

test('should work', async ({ mount }) => {
  const returnedMessage = 'Hello World'
  const component = await mount(<App message={returnedMessage} />)
  await expect(component).toHaveText('Hello World')
})