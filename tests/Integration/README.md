# Integration Tests

Test how different classes interact. For instance, if testing an API endpoint, test that it can (say) use another class in the project as part of its process.