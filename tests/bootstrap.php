<?php
require_once dirname(__FILE__) . '/env.php';
require_once dirname(__FILE__) . '/TestCase.php';
/**
 * Prevent the error
 * Patchwork\Exceptions\DefinedTooEarly: The file that defines wp_remote_get() was included earlier than Patchwork. Please reverse this order to be able to redefine the function in question.
 */
require_once dirname(__FILE__, 2) . '/vendor/antecedent/patchwork/Patchwork.php';
require_once dirname(__FILE__, 2) . '/vendor/autoload.php';
require_once dirname(__FILE__, 2) . '/www/wordpress/wp-load.php';
require_once dirname(__FILE__, 2) . '/www/wordpress/wp-admin/includes/plugin.php';
require_once dirname(__FILE__, 2) . '/www/wordpress/wp-settings.php';
require_once dirname(__FILE__, 2) . '/plugin-name.php';