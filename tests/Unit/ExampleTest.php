<?php

uses(BrainMonkeyTestCase::class);

describe('Settings Definitions: Unit', function() {
  it('defines BOILERPLATE_SETTINGS', function() {
    $test = 'boilerplate-settings';
    $constant = BOILERPLATE_SETTINGS;
    expect($constant)
      ->toBeString()
      ->toEqual($test);
  });
});