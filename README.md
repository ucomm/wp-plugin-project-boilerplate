# WP Plugin Development Boilerplate 
This repo is a boilerplate for doing quick localdev environments managed with composer and docker.
By default it will create images to install wordpress/mysql. Webpack is used to bundle and manage assets.

## git flow
We use [git flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow). Git flow encourages a branching git strategy with several types of branches
- master (for production)
- develop (pre-production)
- feature/* (new features)
- hotfix/* (bug fixes)
- release/* (tagged releases merging into master)

[You can install git flow here](https://github.com/nvie/gitflow/wiki/Installation)

### Typical development workflow starting
- create a new project using composer
  - `git clone git@bitbucket.org:ucomm/wp-plugin-project-boilerplate.git <new-plugin-name>`
  - change the `name` and `description` of the project in the composer.json file. The name is especially important because it is the name that will be used to `require` this project using composer.
- `git flow init -d`
- create a new bitbucket repo under the ucomm team account
- `git flow feature start {feature_name}` this starts a new branch
- work on stuff...
- `git flow feature finish {feature_name}` this merges the branch into `develop`
- test the `develop` branch and share it with others for approval

**See below for instructions on creating hooks for jenkins and bitbucket pipelines for automatic deployment/package management**

### Creating releases
Tags **must** follow the [semver system](http://semver.org/). Follow these steps to complete a release
- on the `develop` branch
  - check `git tag` for the most recent version number
  - bump the version number in `./{plugin-name}.php`
  - add changelog notes to `./changelog.md`
- commit these changes (usually with something like `git -m 'version bump [skip ci]'`)
- follow the process below to create a new tagged release.
```bash
$ git flow release start {new_tag}
$ git flow release finish {new_tag}
$ # in the first screen add new info or just save the commit message as is
$ # in the second screen type in the same tag you just used and save.
$ git push --tags && git push origin master
$ git checkout develop
``` 

## Usage
### Getting started

This project is intended to get development started quickly. As you develop your individual project, please update this readme with project specific details. If you want, you can use parcel, vite, or some other build system instead of webpack. That's completely fine, you simply need to add documentation. Also please update the changelog with notes about new versions.

NB - If you anticipate needing to use a plugin which needs access to the `vendor` directory, map it as follow. In `docker-compose.yml`, find the `web` image and under `volumes` write `- ./vendor:/var/www/html/content/plugins/{plugin_name}/vendor`. This gives both the current theme and plugin the vendor folder.

### To get a project running.

```bash
$ composer install # <- first time only
$ nvm use
$ npm install
$ docker-compose up # <- in one terminal
$ npm run dev # <- in another terminal
```

### UComm composer dependencies
To add any of our projects (plugins, themes, etc...) they need to be added as dependencies. To do that, you will need to first add them to the `composer.json` "repositories" array. To do that, add the `vcs` type key and the git URL of the project. For example:

```
{
   "repositories": [
    {
      "type": "composer",
      "url": "https://wpackagist.org"
    },
    {
      "type": "vcs",
      "url": "git@bitbucket.org:ucomm/uconn-health-clinical.git"
    }
    // other dependencies
  ] 
}
```
These can then be required with `composer require ucomm/project-name`.

### Local asset development

This boilerplate comes with a minimal webpack config file to bundle JS and CSS assets. You will likely need to adjust the config as you go. That's ok, just (as usual) comment the code if there's anything really unique. 

### Viewing a project.
This project will be available at [localhost](http://localhost).

### Accessing containers
To access a particular docker container, find the container name and then enter an interactive terminal.
```bash
$ docker ps # to get the container name
$ docker exec -it container_name bash
```
### Debugging Wordpress
Wordpress debug logs can be found inside the web container at `/etc/httpd/logs/error_log`

## Bitbucket
### Creating releases
Assuming you're using git flow, tag the release with the command `git flow release start {version_number}`. Tags must follow the [semver system](http://semver.org/). Follow these steps to complete a release
```bash
$ git tag # check the current tags/versions on the project
$ git flow release start {new_tag}
$ git flow release finish {new_tag}
$ # in the first screen add new info or just save the commit message as is
$ # in the second screen type in the same tag you just used and save.
$ git push --tags && git push origin master
$ git checkout develop
``` 

### Pipelines
This repo has a bitbucket pipelines attached in case you wish to create zip file downloads for tags/branches. You can create built CSS/JS assets in the pipeline as well. Follow the `bitbucket-pipelines.yml` file to see how. **You may exclude files/folders from the zip archive by adding them to composer.json. The syntax is the same as gitignore. You may explicitly include files/directories as well (e.g. !.gitignore, !vendor/*).** To enable pipelines on bitbucket, simply visit the project repo and enable pipelines at `repo -> settings -> Pipelines/settings`.

### Access Keys
Many projects require integration with either Jenkins (below), or our Satis/composer package repo. These require ssh keys. To add keys to a project...
- Find another project with keys (castor is a good example)
- In that project, go to `Settings -> General -> Access Keys` 
- Copy the keys you need
- In the project you're working on, add new keys (typically called Jenkins and Composer for clarity)

## Jenkins

### Building
If you need to build JS/CSS assets on the Jenkins server, you can do so by adding the following to the shell script.

```
#!/bin/bash

# load nvm and choose a version
source ~/.nvm/nvm.sh
nvm use

# install dependencies
npm ci # NB ci is used instead of install on CI/CD platforms

# build your assets. it will set the NODE_ENV process to production.
npm run build
```


## Using wp-project-boilerplate for a plugin

It would be a good idea to keep UComm/WordPress dependencies in require-dev, and only keep functional dependencies in "require".  That way, your package will export with only the required files for your plugin/theme to function, and not include a full WP install. See the [Castor plugin](https://bitbucket.org/ucomm/castor/src) for an example that uses both.

