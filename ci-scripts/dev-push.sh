#!/bin/bash

# the path in the current working directory to the project
source="./"

# paths to project on staging0
sitepath="$COMM_STAGING_SITES_DIR/$SITE_DIRECTORY/public_html"
projectpath="$sitepath/{wp content directory}/{plugins | themes}/{add the theme or plugin slug}"

# push to staging0
rsync -avzO -e ssh --chmod=ugo=rwX --no-perms \
--exclude-from "excludes.txt" \
--delete $source staging0:$projectpath