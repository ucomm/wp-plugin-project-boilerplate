<?php
/*
Plugin Name: Boilerplate Plugin
Description: Minimal WP plugin boilerplate
Author: UComm Web Team
Version: 0.0.1
Text Domain: boilerplate
*/

// rename this file according to the plugin.

use Boilerplate\Admin\Settings\SettingsPage;
use Boilerplate\Assets\ScriptLoader;
use Boilerplate\Assets\StyleLoader;
use Boilerplate\Frontend\Shortcode;
use Boilerplate\Admin\Settings\Options;

if (!defined('WPINC')) {
	die;
}

define( 'PLUGIN_DIR', plugin_dir_path(__FILE__) );
define( 'PLUGIN_URL', plugins_url('/', __FILE__) );

define('BOILERPLATE_SETTINGS', 'boilerplate-settings');
define('BOILERPLATE_SETTINGS_EXAMPLE', BOILERPLATE_SETTINGS . '-example');

require 'lib/Admin/SettingsManager.php';
require 'lib/Admin/SettingsPage.php';
require 'lib/Admin/Options.php';
require 'lib/Assets/Loader.php';
require 'lib/Assets/ScriptLoader.php';
require 'lib/Assets/StyleLoader.php';
// require 'lib/Database/Database.php';
// require 'lib/Dotenv/Dotenv.php';
require 'lib/Endpoints/Controller.php';
require 'lib/Endpoints/Example.php';
// require 'lib/Files/FileManager.php';
require 'lib/Frontend/Shortcode.php';

$settingsPage = new SettingsPage();
$options = new Options();
$scriptLoader = new ScriptLoader();

add_action('wp_enqueue_scripts', [ $scriptLoader, 'enqueue' ]);

add_action('admin_init', [ $options, 'init' ]);
add_action('admin_menu', [ $settingsPage, 'addSettingsPage' ]);
add_action('admin_enqueue_scripts', [ $scriptLoader, 'adminEnqueue' ]);