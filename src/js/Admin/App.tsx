export default function App({ message }: { message: string }) {
  return (
    <div>
      <h1>{message}</h1>
    </div>
  )
}